@extends('layouts.v2')

@section('content')
	<!-- <div class="secondary-navigation-wrapper">
		<div class="secondary-navigation">
			<div class="menu-level-2-about-us-container">
				<span>Our Expertises</span>
			</div>
		</div>
	</div> -->
	<div class="clearfixed" style="min-height:200px">&nbsp;</div>
	<div class="page-content" style="min-height: 350px !important;">
		@if($expertises)
			@foreach($expertises as $key => $item)
				<div class="expert-div">
					<a  class="expert-icon" href="{{ route('frontend.expertises.detail',$item->slug) }}">
						<img src="{{ url('storage/'.$item->icon) }}" alt="{{ $item->name }}" title="{{ $item->name }}"/>
			  			<h3>{{ $item->name }}</h3>
			  		</a>
		  		</div>
			@endforeach
		@endif
	</div>
	

@endsection


	