@extends('layouts.v2')

@section('content')
	
	<div class="clearfixed">&nbsp;</div>
	<div class="page-content" style="min-height: 400px">
		<!-- <div class="contact-left">
			<form method="POST" action="{{ route('post.send.mail') }}">
				{{ csrf_field() }}
				<div class="form-group">
			    	<label for="email">Email Address</label>
			    	<input type="email" class="form-control" id="email" name="email">
			 	</div>
				
				<div class="form-group">
			    	<label for="full_name">Full Name</label>
			    	<input type="text" class="form-control" id="full_name" name="full_name">
			 	</div>
				
				<div class="form-group">
			    	<label for="phone_number">Phone Number</label>
			    	<input type="text" class="form-control" id="phone_number" name="phone_number">
			 	</div>

				<div class="form-group">
			    	<label for="message">Message</label>
			    	<textarea class="form-control" id="message" name="message"></textarea>
			 	</div>

				<button type="submit" class="btn btn-primary"><i class="fa fa-envelope"></i> Send</button>
			</form>
		</div> -->
		<div class="col-sm-6">
			<h3><i class="fa fa-map"></i> Localtion</h3>
			<hr>
			<iframe src="{{ Voyager::setting('map') }}" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
			<div class="clearfixed">&nbsp;</div>
		</div>
		<div class="col-sm-6">
			<h3><i class="fa fa-phone"></i> Contact Us</h3>
			<hr>
			<div class="row">
				<label class="col-md-3">
					<i class="icon-space fa fa-map-marker"></i>Address
				</label>
				<span class="col-md-8">
					: {{ Voyager::setting('address') }}
				</span>
			</div>
			<div class="row">
				<label class="col-md-3">
					<i class="icon-space fa fa-phone"></i>Phone
				</label>
				<span class="col-md-8">
					: {{ Voyager::setting('phone_number') }}
				</span>
			</div>
			<div class="row">
				<label class="col-md-3">
					<i class="icon-space fa fa-envelope"></i>Email
				</label>
				<span class="col-md-8">
					: {{ Voyager::setting('email') }}
				</span>
			</div>

			<div class="row">
				<div class="col-md-12">
					<a href="mailto:{{ Voyager::setting('email') }}" class="btn btn-warning"><i class="icon-space fa fa-envelope"></i> Send Email</a>
				</div>
			</div>

		</div>

	</div>
	<div class="clearfixed">&nbsp;</div>
@endsection 