@extends('layouts.v2')

@section('content')

	@if (!empty($expert->description))
		<div id="section-landing-wrapper-outer">
	@else
		<div id="section-landing-wrapper-outer" style="margin-bottom: 0px;">
	@endif
	
		<?php $background = (!empty($post->image)) ? $post->image : ""; ?>
		<div id="section-landing-wrapper" style="background: url({{url('storage/'. $background)}}) center center / cover; display: block; background-color: #0e4f9b;">

		<article id="post-4" class="post-4 page type-page status-publish has-post-thumbnail hentry">
			<header class="entry-header">
				<h1 class="entry-title">{{$post->title}}</h1></header><!-- .entry-header -->

			<footer class="entry-footer"></footer><!-- .entry-footer -->
		</article><!-- #post-## -->
		</div>
		</div>

	@if (!empty($expert->description))
		<div class="page-content" style="mix-height: 330px">
			{!! $post->excerpt !!}
		</div>
		<div class="clearfixed">&nbsp;</div>
	@endif
	
@endsection 