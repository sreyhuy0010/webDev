<!-- START REVOLUTION SLIDER 5.4.3.2 fullwidth mode -->
	<div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.3.2">
		
		<ul>	<!-- SLIDE  -->
			@if($slides)
				@foreach($slides as $key => $item)	
			<li data-index="rs-51" data-transition="slideup" data-slotamount="7" 
				data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" 
				data-easeout="default" data-masterspeed="300"  
				data-thumb="{{ url('storage/'.$item->image) }}"  
				data-rotate="0"  data-saveperformance="off"  data-title=" 2017 winners" data-param1="" data-param2="" 
				data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" 
				data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->
				<img src="{{ url('storage/'.$item->image) }}"  alt="" title="{{ $item->title }}"  width="1630" height="918" data-bgposition="center center" data-kenburns="on" data-duration="9000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 100" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->

				<!-- LAYER NR. 1 -->
				<div class="tp-caption clp_large   tp-resizeme" 
					 id="slide-51-layer-1" 
					 data-x="center" data-hoffset="-2" 
					 data-y="188" 
					data-width="['auto']"
					data-height="['auto']"
		 
					data-type="text" 
					data-responsive_offset="on" 

					data-frames='[{"delay":500,"split":"chars","splitdelay":0.05,"speed":500,"split_direction":"forward","frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","split":"chars","splitdelay":0.05,"speed":300,"split_direction":"forward","frame":"999","to":"opacity:0;","ease":"Power4.easeOut"}]'
					data-textAlign="['left','left','left','left']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"

					style="z-index: 5; white-space: nowrap; color: rgba(255,255,255,1); letter-spacing: px;">{{$item->title}}</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption clp_small   tp-resizeme" 
					 id="slide-51-layer-2" 
					 data-x="center" data-hoffset="" 
					 data-y="bottom" data-voffset="196" 
					 data-width="['auto']"
					data-height="['auto']"
		 
					data-type="text" 
					data-responsive_offset="on" 
					data-frames='[{"delay":1500,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Expo.easeOut"},{"delay":"wait","speed":600,"frame":"999","to":"x:right;","ease":"Expo.easeOut"}]'
					data-textAlign="['left','left','left','left']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"

					style="z-index: 6; white-space: nowrap; font-weight: 500; color: rgba(255,255,255,1); letter-spacing:0px;">{!! nl2br(e($item->description )) !!}				</div>

				<!-- LAYER NR. 3 -->
				<div class="tp-caption black   tp-resizeme" 
					 id="slide-51-layer-3" 
					 data-x="center" data-hoffset="-1" 
					 data-y="bottom" data-voffset="82" 
								data-width="['auto']"
					data-height="['auto']"
		 
					data-type="text" 
					data-responsive_offset="on" 

					data-frames='[{"delay":1500,"speed":300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"nothing"}]'
					data-textAlign="['left','left','left','left']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"

					style="z-index: 7; white-space: nowrap; font-size: px; line-height:30px; font-weight: 300; color: #ffffff; letter-spacing: px;font-family:Arial;">
					<a href="{{$item->image_url}}" class="standard-btn">READ MORE</a> </div>

				</li>
				@endforeach
			@endif
		</ul>

		<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
			if(htmlDiv) {
				htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
			}else{
				var htmlDiv = document.createElement("div");
				htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
				document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
			}
		</script>
		<div class="tp-bannertimer" style="height: 5px; background: rgba(0,0,0,0.15);"></div>	
	</div>
		<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss=".tp-caption.black,.black{color:#000;text-shadow:none}.tp-caption.clp_large,.clp_large{font-size:60px;line-height:76px;font-weight:700;font-family:\"Open Sans\";color:#ffffff;text-decoration:none;background-color:transparent;border-width:0px;border-color:rgb(0,0,0);border-style:none;text-shadow:2px 2px 2px rgba(2,2,2,0.25);text-transform:uppercase}.tp-caption.clp_small,.clp_small{font-size:24px;line-height:30px;font-weight:300;font-family:\"Open Sans\";color:#ffffff;text-decoration:none;background-color:transparent;border-width:0px;border-color:rgb(0,0,0);border-style:none;text-shadow:2px 2px 2px rgba(2,2,2,0.25)}.tp-caption.clp_copyright,.clp_copyright{font-size:12px;line-height:30px;font-weight:300;font-family:\"Open Sans\";color:#ffffff;text-decoration:none;background-color:transparent;border-width:0px;border-color:rgb(0,0,0);border-style:none}";
			if(htmlDiv) {
				htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
			}else{
				var htmlDiv = document.createElement("div");
				htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
				document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
			}
		</script>
		<script type="text/javascript">
			setREVStartSize({c: jQuery('#rev_slider_2_1'), gridwidth: [966], gridheight: [500], sliderLayout: 'fullwidth'});
						
			var revapi2,
				tpj=jQuery;
			tpj.noConflict();			
			tpj(document).ready(function() {
				if(tpj("#rev_slider_2_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_2_1");
				}else{
					revapi2 = tpj("#rev_slider_2_1").show().revolution({
						sliderType:"standard",
						jsFileLocation:"{{ url('frontend/v2/js') }}",
						sliderLayout:"fullwidth",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
			 							mouseScrollReverse:"default",
							onHoverStop:"off",
							touch:{
								touchenabled:"on",
								touchOnDesktop:"off",
								swipe_threshold: 0.7,
								swipe_min_touches: 1,
								swipe_direction: "horizontal",
								drag_block_vertical: false
							}
							,
							arrows: {
								style:"hermes",
								enable:true,
								hide_onmobile:false,
								hide_onleave:false,
								tmp:'<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div>	<div class="tp-arr-titleholder"></div>	</div>',
								left: {
									h_align:"left",
									v_align:"center",
									h_offset:20,
									v_offset:0
								},
								right: {
									h_align:"right",
									v_align:"center",
									h_offset:20,
									v_offset:0
								}
							}
							,
							bullets: {
								enable:true,
								hide_onmobile:false,
								style:"hermes",
								hide_onleave:false,
								direction:"horizontal",
								h_align:"center",
								v_align:"bottom",
								h_offset:0,
								v_offset:20,
								space:5,
								tmp:''
							}
						},
						visibilityLevels:[1240,1024,778,480],
						gridwidth:966,
						gridheight:500,
						lazyType:"none",
						shadow:0,
						spinner:"spinner0",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}
				
			});	/*ready*/
		</script>
		<script>
			var htmlDivCss = unescape(".hermes.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A30px%3B%0A%09height%3A110px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%7D%0A%0A.hermes.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A15px%3B%0A%09color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20110px%3B%0A%09text-align%3A%20center%3B%0A%20%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%20%20%20%20transition%3Aall%200.3s%3B%0A%20%20%20%20-webkit-transition%3Aall%200.3s%3B%0A%7D%0A.hermes.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce824%22%3B%0A%7D%0A.hermes.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce825%22%3B%0A%7D%0A.hermes.tparrows.tp-leftarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A.hermes.tparrows.tp-rightarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A%0A.hermes%20.tp-arr-allwrapper%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%09width%3A180px%3B%0A%20%20%20%20height%3A140px%3B%0A%20%20%20%20top%3A0px%3B%0A%20%20%20%20left%3A0px%3B%0A%20%20%20%20visibility%3Ahidden%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20-webkit-transform%200.3s%200.3s%3B%0A%20%20transition%3A%20transform%200.3s%200.3s%3B%0A%20%20-webkit-perspective%3A%201000px%3B%0A%20%20perspective%3A%201000px%3B%0A%20%20%20%20%7D%0A.hermes.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20right%3A0px%3Bleft%3Aauto%3B%0A%20%20%20%20%20%20%7D%0A.hermes.tparrows%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20visibility%3Avisible%3B%0A%20%20%20%20%20%20%20%20%20%20%7D%0A.hermes%20.tp-arr-imgholder%20%7B%0A%20%20width%3A180px%3Bposition%3Aabsolute%3B%0A%20%20left%3A0px%3Btop%3A0px%3Bheight%3A110px%3B%0A%20%20transform%3Atranslatex%28-180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-180px%29%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20transition-delay%3A0.3s%3B%0A%7D%0A.hermes.tp-rightarrow%20.tp-arr-imgholder%7B%0A%20%20%20%20transform%3Atranslatex%28180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28180px%29%3B%0A%20%20%20%20%20%20%7D%0A%20%20%0A.hermes.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%20%20%20%20%20%20%20%20%20%20%20%20%0A%7D%0A.hermes%20.tp-arr-titleholder%20%7B%0A%20%20top%3A110px%3B%0A%20%20width%3A180px%3B%0A%20%20text-align%3Aleft%3B%20%0A%20%20display%3Ablock%3B%0A%20%20padding%3A0px%2010px%3B%0A%20%20line-height%3A30px%3B%20background%3A%23000%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.75%29%3B%0A%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20font-weight%3A600%3B%20position%3Aabsolute%3B%0A%20%20font-size%3A12px%3B%0A%20%20white-space%3Anowrap%3B%0A%20%20letter-spacing%3A1px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transform%3A%20rotatex%28-90deg%29%3B%0A%20%20transform%3A%20rotatex%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%2050%25%200%3B%0A%20%20transform-origin%3A%2050%25%200%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%0A%7D%0A.hermes.tparrows%3Ahover%20.tp-arr-titleholder%20%7B%0A%20%20%20%20-webkit-transition-delay%3A%200.6s%3B%0A%20%20transition-delay%3A%200.6s%3B%0A%20%20-webkit-transform%3A%20rotatex%280deg%29%3B%0A%20%20transform%3A%20rotatex%280deg%29%3B%0A%7D%0A%0A.hermes.tp-bullets%20%7B%0A%7D%0A%0A.hermes%20.tp-bullet%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20border-radius%3A50%25%3B%0A%20%20%20%20width%3A16px%3B%0A%20%20%20%20height%3A16px%3B%0A%20%20%20%20background-color%3A%20rgba%280%2C%200%2C%200%2C%200%29%3B%0A%20%20%20%20box-shadow%3A%20inset%200%200%200%202px%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20%20%20-webkit-transition%3A%20background%200.3s%20ease%3B%0A%20%20%20%20transition%3A%20background%200.3s%20ease%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%7D%0A%0A.hermes%20.tp-bullet%3Ahover%20%7B%0A%09%20%20background-color%3A%20rgba%280%2C0%2C0%2C0.21%29%3B%0A%7D%0A.hermes%20.tp-bullet%3Aafter%20%7B%0A%20%20content%3A%20%27%20%27%3B%0A%20%20position%3A%20absolute%3B%0A%20%20bottom%3A%200%3B%0A%20%20height%3A%200%3B%0A%20%20left%3A%200%3B%0A%20%20width%3A%20100%25%3B%0A%20%20background-color%3A%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20box-shadow%3A%200%200%201px%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20-webkit-transition%3A%20height%200.3s%20ease%3B%0A%20%20transition%3A%20height%200.3s%20ease%3B%0A%7D%0A.hermes%20.tp-bullet.selected%3Aafter%20%7B%0A%20%20height%3A100%25%3B%0A%7D%0A%0A");
			var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
			if(htmlDiv) {
				htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
			}
			else{
				var htmlDiv = document.createElement('div');
				htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
				document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
			}
	  	</script>
	</div><!-- END REVOLUTION SLIDER -->
