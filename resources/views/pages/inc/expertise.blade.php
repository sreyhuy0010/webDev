<h2><strong>Our Expertise</strong> and Services</h2>
<span class="line-diamond"></span>
@if($expertises)
	@foreach($expertises as $key => $item)
		<div class="col-sm-3" style="margin-right: 12px !important;">
			<a class="expert-icon" href="{{ route('frontend.expertises.detail', [$item->id, strtolower(str_replace(' ', '-', $item->name))]) }}">
				<img src="{{ url('storage/'.$item->icon) }}" alt="{{ $item->name }}" title="{{ $item->name }}"/>
	  			<h3>{{ $item->name }}</h3>
	  		</a>
  		</div>
	@endforeach
@endif