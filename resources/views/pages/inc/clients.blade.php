<h3><strong>Our Happy Clients</strong></h3>
<div>
    <div class="">
      	<div class="row">
            <div class="span12">
              	<div id="owl-demo1" class="owl-carousel">
              		@if($clients)
              			@foreach($clients as $key => $item)
              				<div class="item"> <img src="{{ url('storage/'.$item->logo) }}" alt="{{ $item->description }}" title="{{ $item->description }}"/> </div>
              			@endforeach
              		@endif
              	</div>
            </div>
			<a href="/client-portfolio" target="_blank" class="view">View the CAMMA Training client lists</a>
      	</div>
    </div>
</div>