
	      <h3><strong>Our Local Trusted Partners</strong></h3>
	      <div id="demo">
	        <div class="">
	          <div class="row">
	            <div class="span12">
	              <div id="owl-demo" class="owl-carousel">
	              	@if($partners)
              			@foreach($partners as $key => $item)
              				<div class="item"> <img src="{{ url('storage/'.$item->logo) }}" alt="{{ $item->name }}" title="{{ $item->name }}"/> </div>
              			@endforeach
              		@endif
	                </div>
	              <a href="/pd-training-in-the-media-and-industry-events" target="_blank" class="view ">View more CAMMA's trusted partners</a> </div>
	          </div>
	        </div>
	      </div>
	    