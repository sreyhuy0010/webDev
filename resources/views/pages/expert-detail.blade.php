@extends('layouts.frontend')

@section('content')
	
	<div class="page-content">
		<section class="top-heading">
		 	<h2><strong>{{$expert->name}}</strong></h2>
		</section>
	</div>
	<div class="clearfixed">&nbsp;</div>
	<section>
		<div class="page-content">
			<img src="{{ url('storage/'.$expert->icon) }}" alt="{{ $expert->name }}" title="{{ $expert->name }}"/>
			{!! $expert->description !!}
		</div>
	</section>
@endsection 