@extends('layouts.frontend')

@section('content')
<div class="clearfixed" style="height: 64px;"></div>
	<section class="top-heading">
	 	<h2><strong>Our Expertise</strong></h2>
	</section>
	<div class="clearfixed">&nbsp;</div>
	<section>
		<div class="container">
			@if($expertises)
				@foreach($expertises as $key => $item)
					<div class="one-fourth product-wrap" style="margin-right: 12px !important;">
						<a  class="expert-icon" href="{{ route('frontend.expertises.detail',$item->slug) }}">
							<img src="{{ url('storage/'.$item->icon) }}" alt="{{ $item->name }}" title="{{ $item->name }}"/>
				  			<h3>{{ $item->name }}</h3>
				  		</a>
			  		</div>
				@endforeach
			@endif
		</div>
	</section>
	<div class="clearfixed" style="height: 330px"></div>

@endsection