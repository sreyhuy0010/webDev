@extends('layouts.v2')

@section('content')
	

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:700%2C500%2C300" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    <div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;">				
    	@include('pages.inc.slide')

        <div class="page-content">
        	@include('pages.inc.who-we-are')
            <div class="clearfixed">&nbsp;</div>
        </div><!-- .page-content -->

        <div class="row" style="background:#1a5c2e !important; padding: 15px 0px;">
            @if($expertises)
                @foreach($expertises as $key => $item)
                    <div class="col-sm-3" style="padding-top:15px;text-align:center;">
                        <a style="background: none;padding-top:5px;" class="expert-icon" href="{{ route('frontend.expertises.detail',$item->slug) }}" >
                            <img src="{{ url('storage/'.$item->icon) }}" alt="{{ $item->name }}" title="{{ $item->name }}"/>
                            <h3 style="color:#FFFFFF";>{{ $item->name }}</h3>
                        </a>
                        <div class="clearfixed"></div>
                    </div>
                @endforeach
            @endif
        </div><!-- #apply-for-grant-wrapper -->

        
        <h3 id="featured-projects-header">LATEST NEWS</h3>
    	@include('pages.inc.news2')


        <h3 id="featured-projects-header">OUR HAPPY CLIENTS</h3>   
        <div id="footer-partners" class="clear" style="text-align:center;">
            @include('pages.inc.client2')

        </div><!-- .page-content -->
        
        <div id="newsletter-sign-up">
            <div class="section-content-wrapper clear">
                <p id="newsletter-sign-up-text">Sign up to keep in touch with all our latest news</p>
            	@include('pages.inc.subscriber2')
            </div> <!-- .page-content -->
        </div><!-- #newsletter-sign-up -->                   
        
        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- #content -->

@endsection