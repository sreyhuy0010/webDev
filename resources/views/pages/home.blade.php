@extends('layouts.v2')

@section('content')
	
	<div style="clear: both;"></div>
	    <div class="graybg clearfix">
	        <div class="container">

	        <section id="ctl00_divHomepageSearch" class="search-section">
	            <div class="row leftmarginlarge">
	                <form method="get" action="/webpages/search.aspx">
	                    <div class="nine columns">
	                        <input type="search" id="txtHomepageSearch" class="search-input" name="q" placeholder="Search for a course"/>
	                    </div>
	                    <div class="three columns columns_nomargin">
	                        <button id="btnHomepageSearch" class="searchBtn" type="submit" title="Search for a course, industry or topic">
	                            <img src="https://pdtraining.com.sg/assets/images2/pdtraining-topsearch-icon.png" 
	                                alt="Top Search Icon" 
	                                title="Top Search Icon" />
	                            &nbsp;
	                            <span>Search</span>
	                        </button>
	                    </div>
	                </form>
	            </div>
	        </section>
	        <!-- top-search-bar ends-->
	        </div>
	    </div>
	            
	    <div class="main-banner">
	  <div class="container">
	    <div class="slide-caption">
	      <div class="container">
	        <div class="slide-caption-left">
	          <h2>The world's leading training &amp; 
	            development provider</h2>
	          <h3>Recognised for</h3>
	          <ul>
	            <li><span class="fa fa-check-circle-o"></span> Quality service, products and learning experiences</li>
	            <li><span class="fa fa-check-circle-o"></span> Innovation that's transforming our industry</li>
	            <li><span class="fa fa-check-circle-o"></span> Remarkable Value &amp; ROI</li>
	          </ul>
	        </div>
	        <div class="slide-caption-right ">
	         <span class="phone_1">
	        	<img src="https://pdtraining.com.sg/assets/images/telephone-icon.png" alt="Company Phone" /> 3158 3955</span>
	          <p> 
	          	<a href="http://pdtrainingglobal.com/" target="_blank">
	          	<img src="https://pdtraining.com.sg/assets/images/globe-icon.png" alt="PD Training International"  /></a> Supporting you <a href="http://pdtrainingglobal.com/" target="_blank"> worldwide.</a></p>
	        </div>
	      </div>
	    </div> 
	  </div>
	</div>

	<!-- Container starts here-->
	<div class="about-text">
	  <div class="container">
	    <p>We offer world-class training &amp; development with a unique model that is focused on each individual's needs and experience and draws on the very best specialist trainers, consultants and resources.</p>
	  </div>
	</div>
	<!-- Container ends here-->
	<div class="logo-carousel-section">
	  <div class="container">
	    <div class="one-half client-container border-right">
	     	@include('pages.inc.partner')
	    </div>
	    <div class="one-half client-container">
	      	@include('pages.inc.clients')
	    </div>
	  </div>
	</div>
	<div class="clear"></div>
	<!-- Our Expertises Area -->
	<div class="products-section">
	  	<div class="container">
	    	@include('pages.inc.expertise')
	  	</div>
	</div>
	<!-- end Expertises -->

	<div class="clear"></div>
	<div class="white-space"></div>
	<div class="clear"></div>
	<div class="training-section-homepage1">
	  <div class="container">
	    @include('pages.inc.news')
	  </div>
	</div>

	<div class="white-content">
	  <div class="container">
	    <h2><strong>Our Teamworks</strong></h2>
	    <span class="line-diamond"></span> 
	  </div>
	</div>
	
	<div class="container">
	  @include('pages.inc.team')
	  <div class="clear:both;"></div>
	</div>
	<div class="clear"></div>
	<div class="management-section">
	  @include('pages.inc.subscriber')
	</div>
	<!-- <div class="clear"></div> -->
	<!-- <div class="pd-training-section">
	  <div class="container">
	    <h4>Why PD Training? </h4>
	    <h5>We are cost effective and amazingly easy to do business with</h5>
	    <div class="dotted-border"></div>
	    <span>"We exist to empower you – our strategy is based on you choosing us – not us selling to you."</span>
	    <p>With the amazing self-service tools that make it faster and easier to plan, coordinate and report on training than ever before, and dedicated, highly-trained Relationship Managers – you'll always have a professional and prompt answer, but never feel like you're being sold to.</p>
	    <div class="labeled-section">
	      <div class="label-left"> <a href="/unique-training-guarantees" target="_blank">Learn about our unique double guarantee</a> </div>
	      <div class="label-right"> <a href="/unique-training-guarantees" target="_blank"><img src="https://pdtraining.com.sg/assets/images/unique-training-guarantees.png" alt="Unique Training Guarantees" /></a> </div>
	    </div>
	    <div class="clear"></div>
	    <div class="title">
	      <h2>PEOPLE IN ROLES ACROSS THE ORGANIZATION CHOOSE PD Training</h2>
	    </div>
	    <div class="dotted-border"></div>
	  </div>
	</div> -->
	<!-- <div class="container"> -->
	  <!--training -->
	  <!-- <div class="training-box">
	    <h2>Training Co-ordinators choose us <span>because we give:</span></h2>
	    <div class="content-box">
	      <ul>
	        <li><a  class="fancybox-media" href="https://player.vimeo.com/video/161142477"><span>EASIER ADMIN</span><br />
	          and Better Reporting </a></li>
	        <li><a href="/express-booking" target="_blank"><span>Express Booking</span><br/>
	          for Public Classes </a></li>
	        <li><a class="fancybox-media" href="https://vimeo.com/152873076" data-videoid="153179798"><span>Instant Quoting</span><br />
	          for In-House Courses</a></li>
	      </ul>
	      <div class="clear"></div>
	    </div>
	  </div> -->
	  <!--training //-->
	  <!--training -->
	  <!-- <div class="training-box">
	    <h2>Teams choose us <span>because we give:</span></h2>
	    <div class="content-box">
	      <ul>
	        <li><a class="fancybox-media" href="https://vimeo.com/152512240" ><span>A GREAT TRAINER</span><br />
	          and learning experience</a></li>
	        <li> <a   href="/orgmenta" target="_blank"> <span>Personalised Training</span><br />
	          with the Orgmenta App </a> </li>
	        <li><a class="fancybox-media"  href="https://vimeo.com/160831669"> <span>Clear ROI</span><br />
	          Reporting in the cloud </a></li>
	      </ul>
	      <div class="clear"></div>
	    </div>
	  </div> -->
	  <!--training //-->
	  <!--training -->
	  <!-- <div class="training-box">
	    <h2>senior leaders choose  us <span>because we give:</span></h2>
	    <div class="content-box">
	      <ul>
	        <li><a class="fancybox-media" href="https://player.vimeo.com/video/161142766"><span>world-class, <br />
	          built in profiling </span></a> </li>
	        <li><a class="fancybox-media" href="https://player.vimeo.com/video/161722700"><span>National &amp; Global <br />
	          Enterprise Partner </span></a></li>
	        <li><a class="fancybox-media" href="https://vimeo.com/156243180"><span>TARGETED TRAINING WITH <br />
	          TRAINER'S COMPANION</span></a></li>
	      </ul>
	      <div class="clear"></div>
	    </div>
	  </div> -->
	  <!--training //-->
	<!-- </div> -->


	<!--footer -->
	<div id="ctl00_MenuBottom2_cta" class="footer" style="margin: 0px !important;">
	    <div class="container">
	        <!-- <div class="bottom-logo">
	            <img src="https://pdtraining.com.sg/assets/images/pdtraining-logo-footer.png" alt="PDTraining logo" />
	        </div>
	        <div class="divider">
	            <img src="https://pdtraining.com.sg/assets/images/hr-homepage-double-line.png" alt="Homepage double line" />
	        </div>

	        <div class="footer-btns">
	            <a id="ctl00_MenuBottom2_hlInhouseQuote" title="You&#39;re only moments away from receiving your In-House Training Instant Quote!" href="https://pdtraining.com.sg/bookings/inhouseex1/quoterequestex1a.aspx" target="_blank">In-House Training - Instant Quote</a>
	            <a href='https://pdtraining.com.sg/contact-us' 
	                title="Have questions?  Email our friendly staff to get quick answers!">
	                Quick Enquiry - Fast Response
	            </a>
	            <a id="ctl00_MenuBottom2_hlPublicBooking" title="You&#39;re only moments away from enrolling in a scheduled training course from PD Training. Do it today!" href="https://pdtraining.com.sg/bookings/publicclassbooking.aspx" target="_blank">Public Classes - Enrol Now</a>
	            <a id="ctl00_MenuBottom2_hlLogin" title="Returning client?  Login in here." href="https://pdtraining.com.sg/bookings" target="_blank">Returning Clients - Login</a>
	        </div>
	        <div class="consultant">
	            <p>Call now to speak with a friendly consultant</p>
	            <h3>3158 3955</h3>
	        </div>
	        <div class="divider">
	            <img src="https://pdtraining.com.sg/assets/images/hr-homepage-double-line.png" alt="Homepage double line" />
	        </div> -->
	        
	        
	        <!--footer links -->
	        <!-- <div class="footer-links">
	            <h6>
	                <img src="https://pdtraining.com.sg/assets/images/bullet-small-polygon-green.png" alt="Booking option bullet point" />
	                Booking options
	            </h6>
	            <ul>
	                <li id="ctl00_MenuBottom2_liPublicClasses"><a id="ctl00_MenuBottom2_hlPublicClassesPage" title="List of all Scheduled Training Courses." href="/public-classes" target="_blank">Public Training Class</a></li>
	                <li>
	                    <a href="https://pdtraining.com.sg/1-hour-intensive-training" 
	                        title="By running these great fun sessions as a series - (once a week or once a month), 
	                        organisations can drive a behavioral change by maintaining a focus on specific areas.">
	                        1-Hour Intensive Session
	                    </a>
	                </li>
	                <li>
	                    <a href="https://pdtraining.com.sg/full-day-short-courses" 
	                    title="This traditional Professional Development Training style is highly effective and gives the participants 
	                        the best opportunity to really immerse themselves in the topics and explore topics in some depth.">
	                        Full Day Group Booking
	                    </a>
	                </li>
	                <li>
	                    <a href="https://pdtraining.com.sg/conference-workshops" 
	                        title=" PD Training boasts over 300 of the best trainers across Singapore that can deliver a tailored version of 
	                        any of our Professional Development Training Courses at your conference.">
	                        Conferences - Presentations/Workshops
	                    </a>
	                </li>
	                <li>
	                    <a href="https://pdtraining.com.sg/3-hour-power-sessions" 
	                        title="Find out why these fast paced sessions are often the best choice for training for your team.">
	                        3-Hour Power Class
	                    </a>
	                </li>
	                
	            </ul>
	        </div> -->
	        <!--footer links //-->

	        <!-- <div class="vertical-line">
	            <img src="https://pdtraining.com.sg/assets/images/vr-homepage-double-line.png"  alt="Homepage double line"/>
	        </div>
	        <div class="follow">
	            <h6>Follow Us:</h6>
	            <a href="https://www.facebook.com/pdtraining" title=" PD Training like us on facebook">
	                <img src="https://pdtraining.com.sg/assets/images/pdt-facebook-gray.png"  alt=" PD Training like us on facebook" title=" PD Training like us on facebook" />
	            </a>
	            <a href="http://twitter.com/pdtraining" title=" PD Training follow us on twitter">
	                <img src="https://pdtraining.com.sg/assets/images/pdt-twitter-gray.png"  alt=" PD Training follow us on twitter" title=" PD Training follow us on twitter" />
	            </a>
	            <a href="http://www.linkedin.com/company/2663597" title=" PD Training connect with us on linkedin">
	                <img src="https://pdtraining.com.sg/assets/images/pdt-linkedin-gray.png"  alt=" PD Training connect with us on linkedin" title=" PD Training connect with us on linkedin" />
	            </a>
	            <a href="http://www.youtube.com/channel/UC8QTzPfptwG6MSiSsm4XtBQ/videos?view=1&amp;flow=grid" title=" PD Training watch us on youtube">
	                <img src="https://pdtraining.com.sg/assets/images/pdt-youtube-gray.png"  alt=" PD Training watch us on youtube" title=" PD Training watch us on youtube" />
	            </a>
	            <a href="https://plus.google.com/116378261642737464705" rel="publisher" title=" PD Training connect with us on google plus">
	                <img src="https://pdtraining.com.sg/assets/images/pdt-google-plus-gray.png"  alt=" PD Training connect with us on google plus" title=" PD Training connect with us on google plus" />
	            </a>
	        </div>
	        <div class="clear"></div> -->
	    </div>
	</div>

@endsection