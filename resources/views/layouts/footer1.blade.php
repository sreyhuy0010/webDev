<div id='wp_menufication'>
			<div class="col-sm-4">
				<li class="item-head menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-84">
					<a class="a-head" href="#">About Us</a>
					<ul class="sub-menu footer-sub-menu">
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85">
							<a href="{{ route('frontend.page.detail', 'who-we-are') }}">Who We Are</a>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-86">
							<a href="{{ route('frontend.page.detail', 'our-organizational-chart') }}">Our Organizational Chart</a>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87">
							<a href="{{ route('frontend.page.detail', 'our-vision') }}">Our Vision</a>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-88">
							<a href="{{ route('frontend.page.detail', 'our-mission') }}">Our Mission</a>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-89">
							<a href="{{ route('frontend.page.detail', 'our-core-value') }}">Our Core Value</a>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90">
							<a href="{{ route('frontend.page.detail', 'message-from-our-management-team') }}">Message form our Management Team</a>
						</li>
					</ul>
				</li>
			</div>
			<div class="col-sm-4">
				<li class=" item-head menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-89">
				<a class="a-head" href="{{ route('frontend.expertise') }}" title="Our Expertises">Our Expertises</a>
					<ul class="sub-menu footer-sub-menu">
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-90">
							<a href="#">Training And Development Programs</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-95">
							<a href="#">Mentoring And Coaching Programs</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96">
							<a href="#">Teamwork And Teambuilding Program</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-97">
							<a href="#">Web Solution</a></li>
					</ul>
				</li>
			</div>
			<div class="col-sm-4">
				<li class="item-head menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-243">
					<a class="a-head" href="{{ route('frontend.page.detail', 'career-with-us') }}" title="career with us">
	                    	Career with Us</a>
						<ul class="sub-menu footer-sub-menu">
							<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-250">
								<a href="#">Career with CamMa</a></li>
							<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-244">
								<a href="#">Career with CamMa's Partners</a></li>
						</ul>
				</li>
			</div>
		</div>