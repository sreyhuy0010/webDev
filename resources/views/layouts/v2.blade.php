<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome to CAMMA Services</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

<!-- This site is optimized with the Yoast SEO plugin v4.7.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="CAMMA SERVICES"/>
<meta name="robots" content="noodp"/>
<link rel="canonical" href="//www.camma.biz/" />
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Home - CAMMA SERVICES" />
<meta property="og:description" content=" " />
<meta property="og:url" content="//www.camma.biz" />
<meta property="og:site_name" content="CAMMA SERVICES" />
<script type='application/ld+json'>{"@context":"\/\/schema.org","@type":"WebSite","@id":"#website","url":"\/\/www.conservationleadershipprogramme.org\/","name":"Conservation Leadership Programme","potentialAction":{"@type":"SearchAction","target":"\/\/www.conservationleadershipprogramme.org\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->

<script type="text/javascript">
	window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"\/\/www.conservationleadershipprogramme.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=2c16857e2c25e6263d47552e945b0c6c"}};
	!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='custom-css'  href="{{ url('frontend/v2/css/custom.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='cff-css'  href="{{ url('frontend/v2/css/cff-style.css?ver=2.4.5') }}" type='text/css' media='all' />
<link rel='stylesheet' id='cff-font-awesome-css'  href="{{ url('frontend/v2/css/font-awesome.min.css?ver=4.5.0')}}" type='text/css' media='all' />
<link rel='stylesheet' id='essential-grid-plugin-settings-css'  href="{{ url('frontend/v2/css/settings.css')}}" type='text/css' media='all' />
<link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=20150312' type='text/css' media='all' />
<link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=20150312' type='text/css' media='all' />
<link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=20150312' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href="{{ url('frontend/v2/css/revolution-settings.css')}}" type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
	.tp-caption a{
		color:#ff7302;
		text-shadow:none;
		-webkit-transition:all 0.2s ease-out;
		-moz-transition:all 0.2s ease-out;
		-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.largeredbtn{font-family:"Raleway",sans-serif;font-weight:900;font-size:16px;line-height:60px;color:#fff !important;text-decoration:none;padding-left:40px;padding-right:80px;padding-top:22px;padding-bottom:22px;background:rgb(234,91,31); background:-moz-linear-gradient(top,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,rgba(234,91,31,1)),color-stop(100%,rgba(227,58,12,1))); background:-webkit-linear-gradient(top,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); background:-o-linear-gradient(top,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); background:-ms-linear-gradient(top,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); background:linear-gradient(to bottom,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#ea5b1f',endColorstr='#e33a0c',GradientType=0 )}.largeredbtn:hover{background:rgb(227,58,12); background:-moz-linear-gradient(top,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,rgba(227,58,12,1)),color-stop(100%,rgba(234,91,31,1))); background:-webkit-linear-gradient(top,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); background:-o-linear-gradient(top,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); background:-ms-linear-gradient(top,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); background:linear-gradient(to bottom,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#e33a0c',endColorstr='#ea5b1f',GradientType=0 )}.fullrounded img{-webkit-border-radius:400px;-moz-border-radius:400px;border-radius:400px}
</style>
<link rel='stylesheet' id='clp-style-css'  href="{{ url('frontend/v2/css/style.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='lightbox-style-css'  href='{{ url("frontend/v2/css/jquery.fancybox.css") }}' type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-default-css'  href='{{ url("frontend/v2/css/default.min.css") }}' type='text/css' media='all' />
<style id='tablepress-default-inline-css' type='text/css'>
.tablepress	.odd td{background-color:#e6e6e6}.tablepress	.even td{background-color:#F2F2F2}td.column-1,td.column-2,td.column-3,td.column-4,td.column-5{border-right:#FFF 2px solid}.tablepress{font-size:13px}.tablepress thead th{background-color:#006c68;color:#FFF}.tablepress thead th:hover{background-color:#00a950!important;color:#FFF}.tablepress a{text-decoration:none;color:#404040}th.sorting_asc,th.sorting_desc{background-color:#00a950!important}
</style>
<link rel='stylesheet' id='menufication-css-css'  href="{{ url('frontend/v2/css/menufication.min.css?ver=20150312')}}" type='text/css' media='all' />
<!-- This site uses the Google Analytics by MonsterInsights plugin v6.1.9 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script type="text/javascript" data-cfasync="false">
	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	var disableStr = 'ga-disable-UA-3750602-26';
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

	__gaTracker('create', 'UA-3750602-26', 'auto');
	__gaTracker('set', 'forceSSL', true);
	__gaTracker('send','pageview');
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type='text/javascript' src='{{ url("frontend/v2/js/jquery.js") }}'></script>
<script type="text/javascript">
	$("#{{$current_page}}").addClass("current-menu-item current_page_item");
</script>
<script type='text/javascript' src='{{ url("frontend/v2/js/jquery-migrate.min.js") }}'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wp_menufication = {
	"element":"#wp_menufication",
	"enable_menufication":"on",
	"headerLogo":"","headerLogoLink":"",
	"menuLogo":"\/\/www.conservationleadershipprogramme.org\/media\/2015\/01\/conservation-leadership-programme-logo-small.png",
	"menuText":"",
	"triggerWidth":"770",
	"addHomeLink":null,
	"addHomeText":"",
	"addSearchField":null,
	"hideDefaultMenu":"on",
	"onlyMobile":null,
	"direction":"left",
	"theme":"dark",
	"childMenuSupport":"on",
	"childMenuSelector":"sub-menu, children",
	"activeClassSelector":"current-menu-item, current-page-item, active",
	"enableSwipe":"on",
	"doCapitalization":null,"supportAndroidAbove":"3.5",
	"disableSlideScaling":null,
	"toggleElement":"",
	"customMenuElement":"",
	"customFixedHeader":"",
	"addToFixedHolder":"",
	"page_menu_support":null,
	"wrapTagsInList":"",
	"allowedTags":"DIV, NAV, UL, OL, LI, A, P, H1, H2, H3, H4, SPAN, FORM, INPUT, SEARCH",
	"customCSS":"",
	"is_page_menu":"",
	"enableMultiple":"",
	"is_user_logged_in":""
};
/* ]]> */
</script>
<script type='text/javascript' src="{{ url('frontend/v2/js/jquery.menufication.min.js?ver=2c16857e2c25e6263d47552e945b0c6c')}}"></script>
<script type='text/javascript' src="{{ url('frontend/v2/js/menufication-setup.js')}}"></script>
<script type='text/javascript' src="{{ url('frontend/v2/js/lightbox.js?ver=2.1.0.2')}}"></script>
<script type='text/javascript' src="{{ url('frontend/v2/js/jquery.themepunch.tools.min.js?ver=2.1.0.2')}}"></script>
<script type='text/javascript' src="{{ url('frontend/v2/js/jquery.themepunch.revolution.min.js?ver=5.4.3.2')}}"></script>
<script type='text/javascript' src="{{ url('frontend/v2/js/jquery.easing.1.3.js')}}"></script>

<script type="text/javascript" src="{{ url('frontend/v2/js/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{ url('frontend/v2/js/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{ url('frontend/v2/js/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{ url('frontend/v2/js/revolution.extension.navigation.min.js')}}"></script>


<style type="text/css">.myfixed { margin:0 auto!important; float:none!important; border:0px!important; background:none!important; max-width:100%!important; }
	#mysticky-nav 
	{
		 width:100%!important;  position: static;
	}
	 .wrapfixed 
	{ 
		position: fixed!important;
		top:0px!important; 
		left: 0px!important; 
		margin-top:0px!important;  
		z-index: 1000000; 
		-webkit-transition: 0s; 
		-moz-transition: 0s; 
		-o-transition: 0s; 
		transition: 0s;   
		-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=95)";
		filter: alpha(opacity=95); 
		opacity:.95; 
		background-color: #1a5c2e!important;  
	}
	@media (max-width: 980px) 
	{
		.wrapfixed 
		{
			position: static!important; 
			display: none!important;
			}
		}
	</style>
		<script type="text/javascript">
		var ajaxRevslider;
		
		jQuery(document).ready(function() {
			// CUSTOM AJAX CONTENT LOADING FUNCTION
			ajaxRevslider = function(obj) {
			
				// obj.type : Post Type
				// obj.id : ID of Content to Load
				// obj.aspectratio : The Aspect Ratio of the Container / Media
				// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
				
				var content = "";

				data = {};
				
				data.action = 'revslider_ajax_call_front';
				data.client_action = 'get_slider_html';
				data.token = '281c9be9d9';
				data.type = obj.type;
				data.id = obj.id;
				data.aspectratio = obj.aspectratio;
				
				// SYNC AJAX REQUEST
				// jQuery.ajax({
				// 	type:"post",
				// 	url:"//www.conservationleadershipprogramme.org/wp-admin/admin-ajax.php",
				// 	dataType: 'json',
				// 	data:data,
				// 	async:false,
				// 	success: function(ret, textStatus, XMLHttpRequest) {
				// 		if(ret.success == true)
				// 			content = ret.data;								
				// 	},
				// 	error: function(e) {
				// 		console.log(e);
				// 	}
				// });
				
				 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
				 return content;						 
			};
			
			// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
			var ajaxRemoveRevslider = function(obj) {
				return jQuery(obj.selector+" .rev_slider").revkill();
			};

			// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
			var extendessential = setInterval(function() {
				if (jQuery.fn.tpessential != undefined) {
					clearInterval(extendessential);
					if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
						jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
						// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
						// func: the Function Name which is Called once the Item with the Post Type has been clicked
						// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
						// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
					}
				}
			},30);
		});
	</script>
	<script>(function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
	</script>
	<style type="text/css">.ssba {
									
		}
		.ssba img
		{
			width: 50px !important;
			padding: 3px;
			border:  0;
			box-shadow: none !important;
			display: inline !important;
			vertical-align: middle;
		}
		.ssba, .ssba a
		{
			text-decoration:none;
			border:0;
			background: none;
			
			font-size: 	20px;
			color: 		#006c68!important;
			font-weight: bold;
		}
	</style>
	<meta name="generator" content="Powered by Slider Revolution 5.4.3.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
	<script type="text/javascript">function setREVStartSize(e){
		try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
			if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
		}catch(d){console.log("Failure at Presize of Slider:"+d)}
	};
	</script>
	<!-- Easy Columns 2.1.1 by Pat Friedl //www.patrickfriedl.com -->
	<link rel="stylesheet" href="{{ url('frontend/v2/css/easy-columns.css')}}" type="text/css" media="screen, projection" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<style type="text/css">
		.main-navigation
		{
			margin-top: 7px;
		}
		.main-navigation a
		{
			padding: 15px 28px !important;
		}
	</style>
</head>

<body class="home page-template-default page page-id-79 page-parent group-blog">
	@if (session('status'))
        <div class="alert alert-success div-success">
            {{ session('status') }}
        </div>
    @endif
<a href="#my-top" class="scrollToTop"></a>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
	<div id="header-wrapper">
	<header id="masthead" class="site-header" role="banner">
    <div id="header-top-band">
    <div id="site-logo">
        <a href="/" title="Conservation Leadership Programme" rel="home">
        	<img class="site-title" src="{{ url('storage/'.Voyager::setting('logo')) }}" style="width: 210px;">
        </a>
     </div><!-- #site-logo -->
    <div id="social-links">
       <a href="{{ Voyager::setting('facebook') }}" id="facebook">&nbsp;</a> 
       <a href="{{ Voyager::setting('linked_in') }}" id="linked-in">&nbsp;</a> 
       <!--<a href="#" id="instagram">&nbsp;</a>--> 
       <a href="{{ Voyager::setting('youtube') }}" id="youtube">&nbsp;</a>
    </div><!-- #site-social -->
    <div id="search-form-wrapper">
    <form role="search" method="get" class="search-form" action="#">
		<label>
			<span class="screen-reader-text">Search for:</span>
			<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
		</label>
		<input type="submit" class="search-submit" value="Search" />
	</form> 
    </div><!-- #search-form-wrapper{ -->
    </div><!-- #header-top-band -->
	@include('layouts.nav2')
</header><!-- #masthead -->
    
</div><!-- #header-wrapper -->

<div id="content" class="site-content">
		
@yield('content')

<!-- <div id="footer-site-map">
	<div class="container">
	<div class="menu-primary-navigation-container">
	<div id="menu-primary-navigation-1" class="menu">
		@include('layouts.footer1')
	</div>
</div> -->
</div>
</div>
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="site-info">
				<p><strong>&copy;2015 -<?php echo date('Y'); ?> &nbsp; CAMMA Services</strong></p>
            	</div>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-2">
            	<div class="site-info">
	            <div class="menu-footer-navigation-container">
	            	<ul id="menu-footer-navigation" class="menu">
	            		<li id="menu-item-506" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-506">
	            			<a href="{{ route('frontend.page.detail','privacy-policy')}}" title="Privacy Policy">Privacy Policy</a>
	            		</li>
						<li id="menu-item-507" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-507">
							<a href="{{ route('frontend.contact-us') }}" title="Conntact Us">Contact Us</a>
						</li>
					</ul>
				</div>  
				</div> 
			</div>         
            <form role="search" method="get" class="search-form" action="//www.conservationleadershipprogramme.org/">

				<label>
					<span class="screen-reader-text">Search for:</span>
					<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
				</label>
				<input type="submit" class="search-submit" value="Search" />
			</form>              
            
            
		</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->
	
</div><!-- #page -->

<!-- Custom Facebook Feed JS -->
<script type="text/javascript">
var cfflinkhashtags = "true";
</script>
<script type="text/javascript">
	function revslider_showDoubleJqueryError(sliderID) {
		var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
		errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
		errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
		errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
		errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
			jQuery(sliderID).show().html(errorMessage);
	}
</script>
<script type='text/javascript' src='{{ url("frontend/v2/js/cff-scripts.js") }}'></script>
<script type='text/javascript'>
	/* <![CDATA[ */
	var mysticky_name = {"mysticky_string":"#site-navigation","mysticky_active_on_height_string":"102","mysticky_disable_at_width_string":"980"};
	/* ]]> */
</script>
<script type='text/javascript' src="{{('frontend/v2/js/mystickymenu.min.js?ver=1.0.0')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/ssba.min.js?ver=2c16857e2c25e6263d47552e945b0c6c')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/skip-link-focus-fix.js?ver=20130115')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/jquery.fancybox.pack.js?ver=2c16857e2c25e6263d47552e945b0c6c')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/lightbox/js/lightbox.js?ver=2c16857e2c25e6263d47552e945b0c6c')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/jquery-lettering-animate/jquery.lettering.animate.compressed.js?ver=2c16857e2c25e6263d47552e945b0c6c')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/jquery-lettering-animate/jquery.lettering-0.6.1.min.js?ver=2c16857e2c25e6263d47552e945b0c6c')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/custom-jquery-orig.js?ver=201702')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/wp-embed.min.js?ver=2c16857e2c25e6263d47552e945b0c6c')}}"></script>
<script type='text/javascript' src="{{('frontend/v2/js/jquery.themepunch.essential.min.js?ver=2.1.0.2')}}"></script>

</body>
</html>
