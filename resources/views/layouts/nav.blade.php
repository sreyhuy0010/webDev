<nav role="main-nav" id="nav" class="menu-new">
    <div class="container">
        <ul style="z-index: 520;">
            <li>
                <a href="{{ url('/') }}" title="CAMMA Services">
                    <span class="fa fa-home" style="font-size:20px;"></span>
                </a>
            </li>
            <li>
                <a href="{{ route('frontend.page', 'about-us') }}" title="about us">
                    About Us
                </a>
            </li>
            <li>
                <a href="{{ route('frontend.expertise') }}" title="Our Expertises">
                    Our Expertises
                </a>                
            </li>
            <li>
                <a href="{{ route('frontend.news-and-events') }}" title="News and Events">
                    News & Events
                </a>
            </li>
            <li>
                <a href="{{ route('frontend.page', 'career-with-us') }}" id="ctl00_Menu2_ahleLearning" title="career with us">
                    Career with Us
                </a>
            </li> 
            <li>
                <a href="{{ route('frontend.contact-us') }}" title="Conntact Us">
                    Contact Us
                </a>
            </li>
        </ul>
    </div>
</nav>