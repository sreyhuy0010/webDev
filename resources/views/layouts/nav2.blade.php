
<nav id="site-navigation" class="main-navigation" role="navigation">
	<button class="menu-toggle">Primary Menu</button>
	<div class="menu-primary-navigation-container">
		<ul id="menu-primary-navigation" class="menu">
			<div id='wp_menufication'>
				<li id="home" class="menu-item">
					<a href="{{ url('/') }}" title="CAMMA Services">
	                    Home
	                </a>
				</li>
				<li id="about-us" class="menu-item">
					<a href="{{ route('frontend.pages') }}" title="about us">About Us</a>
					<ul class="sub-menu">
						<li id="menu-item-85" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85">
							<a href="{{ route('frontend.page.detail', 'who-we-are') }}">Who We Are</a>
						</li>
						<li id="menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-86">
							<a href="{{ route('frontend.page.detail', 'our-organizational-chart') }}">Our Organizational Chart</a>
						</li>
						<li id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87">
							<a href="{{ route('frontend.page.detail', 'our-vision') }}">Our Vision</a>
						</li>
						<li id="menu-item-88" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-88">
							<a href="{{ route('frontend.page.detail', 'our-mission') }}">Our Mission</a>
						</li>
						<li id="menu-item-89" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-89">
							<a href="{{ route('frontend.page.detail', 'our-core-value') }}">Our Core Value</a>
						</li>
						<li id="menu-item-90" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90">
							<a href="{{ route('frontend.page.detail', 'message-from-our-management-team') }}">Message form our Management Team</a>
						</li>
					</ul>
				</li>
				<li id="our-expertise" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-89">
					<a href="{{ route('frontend.expertise') }}" title="Our Expertises">Our Expertise</a>
					<ul class="sub-menu">
						<li id="menu-item-90" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-90">
							<a href="{{ route('frontend.expertises.detail','training-and-development-programs') }}">Training And Development Programs</a>
						</li>
						<li id="menu-item-95" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-95">
							<a href="{{ route('frontend.expertises.detail','mentoring-and-coaching-program') }}">Mentoring And Coaching Programs</a>
						</li>
						<li id="menu-item-96" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96">
							<a href="{{ route('frontend.expertises.detail','teamwork-and-teambuilding-program') }}">Teamwork And Teambuilding Programs</a>
						</li>
						<li id="menu-item-97" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-97">
							<a href="{{ route('frontend.expertises.detail','web-solution') }}">Web Solution</a>
						</li>
					</ul>
				</li>
				<li id="news-events" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-98">
					<a href="{{ route('frontend.news-and-events') }}" title="News and Events">
                    	News & Events
                	</a>
				</li>
				<li id="career-with-us" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-243">
					<a href="{{ route('frontend.page.detail', 'career-with-us') }}" id="ctl00_Menu2_ahleLearning" title="career with us">
                    	Career with Us
                	</a>
					<ul class="sub-menu">
						<li id="menu-item-250" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-250">
							<a href="{{ route('frontend.page.detail', 'career-with-camma') }}">Career with CamMA</a></li>
						<li id="menu-item-244" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-244">
							<a href="{{ route('frontend.page.detail', 'career-with-cammas-partner') }}">Career with CamMA’s Partner</a>
						</li>
					</ul>
				</li>
				<li id="cfr" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-86">
					<a href="{{ route('frontend.page.detail', 'cfr') }}">CFR</a>
				</li>
				<li id="galleries" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-98">
					<a href="{{ route('frontend.galleries')}}" title="Galleries">
                    	Galleries
                	</a>
				</li>

				<li id="contact-us" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-104">
					<a href="{{ route('frontend.contact-us') }}" title="Conntact Us">
                    Contact Us
                	</a>
				</li>
				
			</div>
		</ul>
	</div>		
</nav><!-- #site-navigation -->