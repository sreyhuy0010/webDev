jQuery(document).ready(function () {
	jQuery("#owl-demo").owlCarousel({
		autoPlay: 3000,
		items: 3,
		itemsDesktop: [1199, 3],
		itemsDesktopSmall: [979, 3],
		navigation: true,
		navigationText: ["prev", "next"],
		rewindNav: true,
		scrollPerPage: false,
		pagination: false,
		paginationNumbers: false
	});

	jQuery("#owl-demo1").owlCarousel({
		autoPlay: 3000,
		items: 3,
		itemsDesktop: [1199, 3],
		itemsDesktopSmall: [979, 3],
		navigation: true,
		navigationText: ["prev", "next"],
		rewindNav: true,
		scrollPerPage: false,
		pagination: false,
		paginationNumbers: false
	});


});
