/* Foundation v2.2.1 http://foundation.zurb.com */
$.noConflict();
jQuery(document).ready(function ($) {

    $('.btn-classcalendar').html('Training Calendar');
    $('.btn-classcalendar').html('Training Calendar');

    //$('#coursebuttons>dd:nth-child(4) a').html('Public Class');
    $('#coursebuttons>dd:nth-child(4) a').addClass('datesicon');

    $('#testimonials').masonry({
        itemSelector: '.box',
        gutterWidth: 10,
    });

    $('.divTestimonials').masonry({
        itemSelector: '.box',
        gutterWidth: 10,
    });

    /*
    
    Center child in parent.
    
    $(".object").center({
    horizontal: false // only vertical
    });
    */

    /* Load Schedules for apps */

    $("#aspnetAppScheduleBrisbane").load("/courselists/schedulepbcategory.aspx?c=11&t=22&l=11 #main .tblCourses");
    $("#aspnetAppScheduleSydney").load("/courselists/schedulepbcategory.aspx?c=11&t=22&l=9 #main .tblCourses");
    $("#aspnetAppScheduleParramatta").load("/courselists/schedulepbcategory.aspx?c=11&t=22&l=54 #main .tblCourses");
    $("#aspnetAppScheduleMelbourne").load("/courselists/schedulepbcategory.aspx?c=11&t=22&l=10 #main .tblCourses");
    $("#aspnetAppScheduleCanberra").load("/courselists/schedulepbcategory.aspx?c=11&t=22&l=12 #main .tblCourses");
    $("#aspnetAppSchedulePerth").load("/courselists/schedulepbcategory.aspx?c=11&t=22&l=13 #main .tblCourses");
    $("#aspnetAppScheduleAdelaide").load("/courselists/schedulepbcategory.aspx?c=11&t=22&l=14 #main .tblCourses");
    $("#aspnetAppScheduleHive").load("/courselists/schedulepbcategory.aspx?c=11&t=22&l=56 #main .tblCourses");

    /* load schedules for pd courses */

    $("#aspnetPublicScheduleBrisbane").load("/courselists/schedulepbcategory.aspx?c=1&t=10&l=4 #main .tblCourses");
    $("#aspnetPublicScheduleSydney").load("/courselists/schedulepbcategory.aspx?c=1&t=10&l=2 #main .tblCourses");
    $("#aspnetPublicScheduleParramatta").load("/courselists/schedulepbcategory.aspx?c=1&t=10&l=54 #main .tblCourses");
    $("#aspnetPublicScheduleMelbourne").load("/courselists/schedulepbcategory.aspx?c=1&t=10&l=3 #main .tblCourses");
    $("#aspnetPublicScheduleCanberra").load("/courselists/schedulepbcategory.aspx?c=1&t=10&l=8 #main .tblCourses");
    $("#aspnetPublicSchedulePerth").load("/courselists/schedulepbcategory.aspx?c=1&t=10&l=6 #main .tblCourses");
    $("#aspnetPublicScheduleAdelaide").load("/courselists/schedulepbcategory.aspx?c=1&t=10&l=7 #main .tblCourses");
    $("#aspnetPublicScheduleHive").load("/courselists/schedulepbcategory.aspx?c=1&t=10&l=55 #main .tblCourses");

    /* load schedules for fish courses */

    $("#aspnetFishScheduleBrisbane").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=4 #main .tblCourses");
    $("#aspnetFishScheduleSydney").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=2 #main .tblCourses");
    $("#aspnetFishScheduleParramatta").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=54 #main .tblCourses");
    $("#aspnetFishScheduleMelbourne").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=3 #main .tblCourses");
    $("#aspnetFishScheduleCanberra").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=8 #main .tblCourses");
    $("#aspnetFishSchedulePerth").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=6 #main .tblCourses");
    $("#aspnetFishScheduleAdelaide").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=7 #main .tblCourses");
    /* New Zealand */
    $("#aspnetFishScheduleAuckland").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=72 #main .tblCourses");
    $("#aspnetFishScheduleWellington").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=73 #main .tblCourses");
    $("#aspnetFishScheduleChristchurch").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=79 #main .tblCourses");
    /* Singapore */
    $("#aspnetFishScheduleSingapore").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=72 #main .tblCourses");
    /* Philippines */
    $("#aspnetFishScheduleManila").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=72 #main .tblCourses");
    /* Malaysia */
    $("#aspnetFishScheduleKualaLumpur").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=72 #main .tblCourses");
    /* Hong Kong */
    $("#aspnetFishScheduleCentral").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=72 #main .tblCourses");
    /* Nigeria */
    $("#aspnetFishScheduleLagos").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=72 #main .tblCourses");
    /* UK */
    $("#aspnetFishScheduleLondon").load("/courselists/schedulepbcategory.aspx?c=23&t=23&l=72 #main .tblCourses");

    /* load schedules for Six Sigma courses */
    
    $("#aspnetSixScheduleBrisbane").load("/courselists/schedulepbcategory.aspx?c=22&t=22&l=4 #main .tblCourses");
    $("#aspnetSixScheduleSydney").load("/courselists/schedulepbcategory.aspx?c=22&t=22&l=2 #main .tblCourses");
    $("#aspnetSixScheduleParramatta").load("/courselists/schedulepbcategory.aspx?c=22&t=22&l=54 #main .tblCourses");
    $("#aspnetSixScheduleMelbourne").load("/courselists/schedulepbcategory.aspx?c=22&t=22&l=3 #main .tblCourses");
    $("#aspnetSixScheduleCanberra").load("/courselists/schedulepbcategory.aspx?c=22&t=22&l=8 #main .tblCourses");
    $("#aspnetSixSchedulePerth").load("/courselists/schedulepbcategory.aspx?c=22&t=22&l=6 #main .tblCourses");
    $("#aspnetSixScheduleAdelaide").load("/courselists/schedulepbcategory.aspx?c=22&t=22&l=7 #main .tblCourses");
    $("#aspnetSixScheduleHive").load("/courselists/schedulepbcategory.aspx?c=22&t=22&l=55 #main .tblCourses");

    /* Development Programmes */

    $('#header').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.top').addClass('active');
    });
    $('#description').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.description').addClass('active');
    });
    $('#feels-like-this').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.feels-like-this').addClass('active');
    });
    $('#needs-analysis').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.needs-analysis').addClass('active');
    });
    $('#analyse-results').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.analyse-results').addClass('active');
    });
    $('#deliver-training').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.deliver-training').addClass('active');
    });
    $('#measure-impact').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.measure-impact').addClass('active');
    });
    $('#recruitment').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.recruitment').addClass('active');
    });
    $('#typical-year-1').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.typical-year-1').addClass('active');
    });
    $('#typical-year-2').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.typical-year-2').addClass('active');
    });
    $('#typical-year-3').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.typical-year-3').addClass('active');
    });
    $('#our-clients').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.our-clients').addClass('active');
    });
    $('#strategic-alliances').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.strategic-alliances').addClass('active');
    });
    $('#corporate-philosophy').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.corporate-philosophy').addClass('active');
    });
    $('#training-solutions').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.training-solutions').addClass('active');
    });
    $('#international-training-organisation').waypoint(function () {
        $('.vertical dd a').removeClass('active');
        $('.international-training-organisation').addClass('active');
    });


    /* 	Replace text until next pass  */

    /*
        $("#ScheduleTab p").each(function() {
            var text = $(this).text();
            text = text.replace("day - day", "day -");
            $(this).text(text);
        });
        $("#OutcomesTab p").each(function() {
            var text = $(this).text();
            text = text.replace("Positive communication leads to positive living, both in and out of the work place.", "");
            $(this).text(text);
        });
    */
    /* Use this js doc for all application specific JS */

    /* 	jQuery('.elearningiframe').removeAttr("scrolling") */


    function equalHeight(group) {
        tallest = 0;
        group.each(function () {
            thisHeight = $(this).height();
            if (thisHeight > tallest) {
                tallest = thisHeight;
            }
        });
        group.height(tallest);
    }

    equalHeight($(".outcomescolumns"));
    equalHeight($(".equalheight"));

    $("#course-tabs").easytabs({
        defaultTab: "dd:first-child",
        panelActiveClass: "active",
        tabActiveClass: "active",
        tabs: "> dl > dd",
        updateHash: false,
        animate: false
        /*		animationSpeed: "fast"
        */
    });

    /* 	$('.qtip td a[href][title]').qtip({ */
    $('.qtip td:nth-child(n+3) a[href][title]').qtip({
        content: 'Click this date to enroll today',
        position: {
            corner: {
                target: 'topMiddle',
                tooltip: 'bottomMiddle'
            }
        },
        style: {
            width: 120,
            padding: 5,
            background: '#6c9e00',
            color: 'white',
            textAlign: 'center',
            border: {
                width: 3,
                radius: 5,
                color: '#80B600'
            },
            tip: 'bottomMiddle',
            name: 'dark' // Inherit the rest of the attributes from the preset dark style
        }
    });

    $('.qtip a.missed-out[href][title]').qtip({
        content: 'Last Chance! Click this date to enroll today',
        position: {
            corner: {
                target: 'topMiddle',
                tooltip: 'bottomMiddle'
            }
        },
        style: {
            width: 120,
            padding: 5,
            background: '#f9e7c5',
            color: 'black',
            textAlign: 'center',
            border: {
                width: 3,
                radius: 5,
                color: '#e19400'
            },
            tip: 'bottomMiddle',
            name: 'cream' // Inherit the rest of the attributes from the preset dark style
        }
    });

    jQuery(".btn-classcalendar").click(function () {
        jQuery(".course-tabs dd a").removeClass("active");
        jQuery(".course-tabs dd:nth-child(4) a").addClass("active");
        jQuery(".course-tabs>ul>li").css("display", "none");
        jQuery(".course-tabs #ScheduleTab").css("display", "block");
    });

    jQuery(".btn-inhouse").click(function () {
        jQuery(".course-tabs dd a").removeClass("active");
        jQuery(".course-tabs dd:nth-child(3) a").addClass("active");
        jQuery(".course-tabs>ul>li").css("display", "none");
        jQuery(".course-tabs #In-HouseTab").css("display", "block");
    });

    /* 	New tab system: */

    jQuery(".btn-classcalendar").click(function () {
        jQuery("#course-tabs dd a").removeClass("active");
        jQuery("#course-tabs dd:nth-child(4) a").addClass("active");
        jQuery("#course-tabs>ul>li").css("display", "none");
        jQuery("#course-tabs #Schedule").css("display", "block");
    });

    jQuery(".btn-inhouse").click(function () {
        jQuery("#course-tabs dd a").removeClass("active");
        jQuery("#course-tabs dd:nth-child(3) a").addClass("active");
        jQuery("#course-tabs>ul>li").css("display", "none");
        jQuery("#course-tabs #In-House").css("display", "block");
    });


    /* Category Pages */

    /* 	var target = $(this).attr("href"); */

    jQuery("a.in-house-training").click(function () {
        jQuery(".training-options").removeClass("hide");
        jQuery("#In-House-Training").slideDown("slow");
        jQuery(".getstarted").fadeOut('slow');
    });
    jQuery("a.public-classes").click(function () {
        jQuery(".training-options").removeClass("hide");
        jQuery("#Public-Classes").slideDown("slow");
        jQuery(".getstarted").fadeOut("slow");
    });

    jQuery("a.public-schedule").click(function () {
        jQuery("#Public-Schedule").slideDown("slow");
        jQuery(".getstarted").fadeOut("slow");
    });
    jQuery("#Public-Schedule .close").click(function () {
        jQuery("#Public-Schedule").slideUp("slow");
    });
    jQuery(".training-options .close").click(function () {
        jQuery(".click").css("display", "block");
        jQuery("#In-House-Training").slideUp("slow");
        jQuery("#Public-Classes").slideUp("slow");
    });


    /* Smooth Scroll to internal link */


    $(".steplabels .step-1").hover(
	  function () {
	      $(".steptext .step-1").css("display", "block");
	      $(".steptext .process").css("display", "none");
	  },
	  function () {
	      $(".steptext .step-1").css("display", "none");
	      $(".steptext .process").css("display", "block");
	  }
	);
    $(".steplabels .step-2").hover(
	  function () {
	      $(".steptext .step-2").css("display", "block");
	      $(".steptext .process").css("display", "none");
	  },
	  function () {
	      $(".steptext .step-2").css("display", "none");
	      $(".steptext .process").css("display", "block");
	  }
	);
    $(".steplabels .step-3").hover(
	  function () {
	      $(".steptext .step-3").css("display", "block");
	      $(".steptext .process").css("display", "none");
	  },
	  function () {
	      $(".steptext .step-3").css("display", "none");
	      $(".steptext .process").css("display", "block");
	  }
	);
    $(".steplabels .step-4").hover(
	  function () {
	      $(".steptext .step-4").css("display", "block");
	      $(".steptext .process").css("display", "none");
	  },
	  function () {
	      $(".steptext .step-4").css("display", "none");
	      $(".steptext .process").css("display", "block");
	  }
	);
    $(".steplabels .step-5").hover(
	  function () {
	      $(".steptext .step-5").css("display", "block");
	      $(".steptext .process").css("display", "none");
	  },
	  function () {
	      $(".steptext .step-5").css("display", "none");
	      $(".steptext .process").css("display", "block");
	  }
	);
    $(".steplabels .step-6").hover(
	  function () {
	      $(".steptext .step-6").css("display", "block");
	      $(".steptext .process").css("display", "none");
	  },
	  function () {
	      $(".steptext .step-6").css("display", "none");
	      $(".steptext .process").css("display", "block");
	  }
	);

    /*
    $('a[href^="#"]').bind('click.smoothscroll',function (e) {
        e.preventDefault();
        var target = this.hash,
            $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 500, 'swing', function () {
            window.location.hash = target;
        });
    });
    */


    /* Simple accordion */

    //Add Inactive Class To All Accordion Headers
    jQuery('.accordion-header').toggleClass('inactive-header');

    //Set The Accordion Content Width
    /*
        var contentwidth = $('.accordion-header').width();
        $('.accordion-content').css({'width' : contentwidth });
    */

    /*
        //Open The First Accordion Section When Page Loads - uncomment when needed
        $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
        $('.accordion-content').first().slideDown().toggleClass('open-content');
    */

    // The Accordion Effect


    jQuery('.accordion-header').click(function () {
        if ($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle('fast').toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle('fast').toggleClass('open-content');
        }

        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle('fast').toggleClass('open-content');
        }
    });


    /* rotator */

    if (jQuery.isFunction(jQuery.orbit)) {

        jQuery('#featuredslides').orbit({
            timer: false,
            bullets: true,
            directionalNav: true,
            captions: true,
            animationSpeed: 800,
            animation: 'fade',
            advanceSpeed: 6000
        });

    }

    jQuery('.conditions').click(function () {
        $('#myModal').reveal();
    });


    var filetypes = /\.(zip|exe|pdf|doc*|xls*|ppt*|mp3)$/i;
    var baseHref = '';
    if (jQuery('base').attr('href') != undefined)
        baseHref = jQuery('base').attr('href');
    jQuery('a').each(function () {
        var href = jQuery(this).attr('href');
        if (href && (href.match(/^https?\:/i)) && (!href.match(document.domain))) {
            jQuery(this).click(function () {
                var extLink = href.replace(/^https?\:\/\//i, '');
                _gaq.push(['_trackEvent', 'External', 'Click', extLink]);
                if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
                    setTimeout(function () { location.href = href; }, 200);
                    return false;
                }
            });
        }
        else if (href && href.match(/^mailto\:/i)) {
            jQuery(this).click(function () {
                var mailLink = href.replace(/^mailto\:/i, '');
                _gaq.push(['_trackEvent', 'Email', 'Click', mailLink]);
            });
        }
        else if (href && href.match(filetypes)) {
            jQuery(this).click(function () {
                var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
                var filePath = href;
                _gaq.push(['_trackEvent', 'Download', 'Click-' + extension, filePath]);
                if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
                    setTimeout(function () { location.href = baseHref + href; }, 200);
                    return false;
                }
            });
        }
    });

    /*     scroll menu */

    /*
        var menu = jQuery('#submenu'),
            pos = menu.offset();
    
            jQuery(window).scroll(function(){
                if($(this).scrollTop() > pos.top+menu.height() && menu.hasClass('default')){
                    menu.fadeOut('fast', function(){
                        $(this).removeClass('default').addClass('fixed').fadeIn('slow');
                    });
                } else if($(this).scrollTop() <= pos.top && menu.hasClass('fixed')){
                    menu.fadeOut('fast', function(){
                        $(this).removeClass('fixed').addClass('default').fadeIn('fast');
                    });
                }
            });
    */

    /* 	Filter courses */

    $("#filter").bind("change keyup input", function () {

        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;

        // Loop through the comment list
        $("#ctl00_contentPlaceHolder_ListView1_groupPlaceholderContainer li").each(function () {

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });

    });

    $("#filtercourses").bind("change keyup input", function () {
        
        //debugger;
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;
        var currentcategoryId = 0;

        // Loop through the comment list
        $(".divCourseContainer ul").each(function () {

            $(this).find('li').each(function () {
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).fadeOut();
                } else {
                    $(this).show();
                    currentcategoryId = $(this).find('input:hidden').val();
                }
            });

            if (currentcategoryId > 0) {
                $(this).parent('div').parent('div').parent('div').find('.coc_titlebg').show();
            }
            else {
                $(this).parent('div').parent('div').parent('div').find('.coc_titlebg').hide();
            }

            currentcategoryId = 0;
             // If the list item does not contain the text phrase fade it out

        });

    });

    $("#groupfilter").bind("change keyup input", function () {

        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;
        $(".search").show();
        $("#PlaceholderContainer").hide();

        // Loop through the comment list
        $("#ctl00_contentPlaceHolder_ListView2_groupPlaceholderContainer li").each(function () {

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }

            if ($(this).text().search(new RegExp(filter, "i")) == 0) {
                $(".search").hide();
                $("#PlaceholderContainer").show();
            } else {

            }

        });

    });


    $("#elearningfilter").bind("change keyup input", function () {

        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;

        // Loop through the comment list
        $("#ctl00_ContentPlaceHolder1_gvUnits tr").each(function () {

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });

    });

    //elearning new filter.
    $("#elearningbindFilter").bind("change keyup input", function () {
        debugger;
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;

        // Loop through the comment list
        $(".courseunitlistBox tr").each(function () {

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });

    });


    /* 	Filter courses form */

    $("#Schedulefilter").keyup(function () {

        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;

        // Loop through the comment list
        $("#Public-Schedule tr").each(function () {

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });
    });

    /* 	Filter Public Schedule */

    $("#PublicSchedulefilter").keyup(function () {

        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;
        var currentcategoryId = 0;

        // Loop through the comment list
        $(".tblCourses tr").each(function () {

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                currentcategoryId = $(this).find('input:hidden').val();
            }
        });
        if (currentcategoryId > 0) {
                $(this).parent('div').find('.coc_titlebg').show();
            }
            else {
                $(this).parent('div').find('.coc_titlebg').hide();
            }

            currentcategoryId = 0;
            // If the list item does not contain the text phrase fade it out
        
    });


    $('.option a.active').click(function () {
        var $this = $(this);
        $this.text('More Details');
    });

    $(".option a").click(function () {
        var $this = $(this);
        var target = $this.data('content');
        $(this).addClass('active');
        $('.option a').not($this).each(function () {
            $(this).removeClass('active');
            var $other = $(this);
            var otherTarget = $other.data('content');
            $(otherTarget).hide();
        });
        $(target).animate({ opacity: "toggle" }, 0);
        return false;
    });



    /*     Simple Accordion */

    var allPanels = $('.accordion > dd').hide();
    var allPanelTitles = $('.accordion > dt').removeClass('active');

    $('.accordion > dt > a').click(function () {
        $this = $(this);
        $target = $this.parent().next();
        $this.parent().addClass('active');

        if (!$target.hasClass('active')) {
            allPanels.removeClass('active').slideUp('slow');
            allPanelTitles.removeClass('active');
            $target.addClass('active').slideDown('slow');
            $this.parent().addClass('active');
        }
        return false;
    });



    /* 	email obfuscator */
    $('a.email').each(function () {
        e = this.rel.replace('../../index.html', '@');
        this.href = 'mailto:' + e;
        $(this).text(e);
    });

    /* Scroller for Call to actions */

    (function ($) { $.fn.jScroll = function (e) { var f = $.extend({}, $.fn.jScroll.defaults, e); return this.each(function () { var a = $(this); var b = $(window); var c = new location(a); b.scroll(function () { a.stop().animate(c.getMargin(b), f.speed) }) }); function location(d) { this.min = d.offset().top; this.originalMargin = parseInt(d.css("margin-top"), 10) || 0; this.getMargin = function (a) { var b = d.parent().height() - d.outerHeight(); var c = this.originalMargin; if (a.scrollTop() >= this.min) c = c + f.top + a.scrollTop() - this.min; if (c > b) c = b; return ({ "marginTop": c + 'px' }) } } }; $.fn.jScroll.defaults = { speed: "slow", top: 10 } })(jQuery);

    //$(function () {
    //    $(".scroll").jScroll({ speed: "slow" });
    //});

    $(".scroller").click(function (event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: $(this.hash).offset().top - 20 }, 400);
    });

    $(".coursescroller").click(function (event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: $(this.hash).offset().top - 100 }, 400);
    });


    /* TABS --------------------------------- */
    /* Remove if you don't need :) */

    function activateTab($tab) {
        var $activeTab = $tab.closest('dl').find('a.active');
		contentLocation = $tab.attr("href") + 'Tab';

        // Strip off the current url that IE adds
        contentLocation = contentLocation.replace(/^.+#/, '#');

        //Make Tab Active
        $activeTab.removeClass('active');
        $tab.addClass('active');

        //Show Tab Content
        $(contentLocation).closest('.tabs-content').children('li').hide();
        $(contentLocation).css('display', 'block');
    }

    $('dl.tabs dd a').live('click', function (event) {
        activateTab($(this));
        setTabColor();
    });

    if (window.location.hash) {
        activateTab($('a[href="' + window.location.hash + '"]'));
        $.foundation.customForms.appendCustomMarkup();
    }

    $(window).hashchange(function () {

        activateTab($('a[href="' + window.location.hash + '"]'));

    })


    /* ALERT BOXES ------------ */
    $(".alert-box").delegate("a.close", "click", function (event) {
        event.preventDefault();
        $(this).closest(".alert-box").fadeOut(function (event) {
            $(this).remove();
        });
    });

    /* PLACEHOLDER FOR FORMS ------------- */
    /* Remove this and jquery.placeholder.min.js if you don't need :) */

    $('input, textarea').placeholder();

    /* TOOLTIPS ------------ */
    $(this).tooltips();



    /* UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE6/7/8 SUPPORT AND ARE USING .block-grids */
    //	$('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'left'});
    //	$('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'left'});
    //	$('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'left'});
    //	$('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'left'});



    /* DROPDOWN NAV ------------- */

    var lockNavBar = false;
    $('.nav-bar a.flyout-toggle').live('click', function (e) {
        e.preventDefault();
        var flyout = $(this).siblings('.flyout');
        if (lockNavBar === false) {
            $('.nav-bar .flyout').not(flyout).slideUp(500);
            flyout.slideToggle(500, function () {
                lockNavBar = false;
            });
        }
        lockNavBar = true;
    });
    if (Modernizr.touch) {
        $('.nav-bar>li.has-flyout>a.main').css({
            'padding-right': '33px',
            'padding-left': '6px'
        });
        $('.nav-bar>li.has-flyout>a.flyout-toggle').css({
            'border-left': '1px dashed #AAA',
            'padding-left': '6px',
            'padding-right': '6px',
        });
    } else {
        $('.nav-bar>li.has-flyout').hover(function () {
            $(this).children('.flyout').show();
        }, function () {
            $(this).children('.flyout').hide();
        })
    }


    /* DISABLED BUTTONS ------------- */
    /* Gives elements with a class of 'disabled' a return: false; */

});