<?php

namespace Theavuth\Http\Controllers\Frontend;

use Theavuth\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Theavuth\Client;
use Theavuth\Expertise;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\Post;

class ContactUsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       \View::share('current_page', 'contact-us');
    }

    public function index()
    {
        return view('pages.contact-us');
    }

}
