<?php

namespace Theavuth\Http\Controllers\Frontend;

use Theavuth\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Theavuth\Client;
use Theavuth\Expertise;
use Theavuth\Post;
use TCG\Voyager\Models\Page;

class PageController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::share('current_page', 'about-us');
    }

    public function getAllPage()
    {
        $slug = "";
        $page = "";
        return view('pages.about-us2', compact('page', 'slug'));
    }

    public function getPage($slug)
    {
        $page = Page::where('slug', $slug)->first();
        return view('pages.about-us2', compact('page', 'slug'));
    }

}
