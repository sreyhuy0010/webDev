<?php

namespace Theavuth\Http\Controllers\Frontend;

use Theavuth\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MailController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    protected $email = "";
    protected $full_name = "";
    protected $to = "";

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       \View::share('current_page', 'contact-us');

       $this->email = "";
       $this->full_name = "";
       $this->to = "";
    }

    public function send(Request $request)
    {
        $data = array_except($request->all(), ['_token']);
        $data += ['to' => env('EMAIL_SEND_TO', 'info@camma.biz')];
        $this->email = $data['email'];
        $this->full_name = $data['full_name'];
        $this->to = $data['to'];

        \Mail::send('emails.email-contact', ['data' => $data], function ($message) use ($data)
        {
            $message->from($data['email'], $data['full_name']);
            $message->to(env('MAIL_USERNAME', 'sreyhuy0010@gmail.com'));
            $message->subject($this->full_name);

        });

        return route('frontend.contact-us')->with('status', 'The email was successfully sent.');;

    }
}
