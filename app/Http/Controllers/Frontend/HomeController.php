<?php

namespace Theavuth\Http\Controllers\Frontend;

use Theavuth\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Theavuth\Client;
use Theavuth\Expertise;
use Theavuth\Partner;
use TCG\Voyager\Models\Post;
use Theavuth\Team;
use TCG\Voyager\Models\Page;
use Theavuth\Slide;
class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::share('current_page', 'home');
    }

    public function index()
    {
        $clients = Client::orderBy('id', 'DESC')
                         ->take(4)
                         ->get();
        $expertises = Expertise::orderBy('id', 'DESC')
                               ->get();
        $partners = Partner::orderBy('id','DESC')
                               ->get();
        $posts = Post::take(3)->orderBy('created_at','DESC')
                               ->get();
        $team = Team::orderBy('created_at','DESC')
                               ->get();
        $who = Page::where('slug', 'who-we-are')->first();

        $what = Page::where('slug', 'what-we-do')->first();

        $slides = Slide::orderBy('id','DESC')
                                ->get();
                            
        return view('pages.home2', compact('clients', 'expertises','partners','posts','team','who','what','slides'));
    }

}
