<?php

namespace Theavuth;

use Illuminate\Database\Eloquent\Model;


class Subscriber extends Model
{
    protected $fillable = ['email', 'full_name', 'phone_number'];
}
