<?php

namespace FWTA\Core\Console\Commands\Datatable;


use Illuminate\Console\GeneratorCommand;
use Yajra\Datatables\Generators\DataTablesMakeCommand as DataTablesMakeCommandBase;

/**
 * Class DataTablesMakeCommand.
 */
class DataTablesMakeCommand extends DataTablesMakeCommandBase
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'module:datatables:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new DataTable Service class.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'DataTable';

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\DataTables';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/stubs/datatables.stub';
    }
}
