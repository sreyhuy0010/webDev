<?php namespace FWTA\Core\Console\Commands;

use Illuminate\Support\Str;
use Pingpong\Modules\Commands\GeneratorCommand;
use Pingpong\Modules\Traits\ModuleCommandTrait;
use Pingpong\Support\Stub;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RepositoryCommand extends GeneratorCommand {

	use ModuleCommandTrait;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'module:make-repository {model} {module}';
	protected $argumentName = 'model';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate new repository for the specified module.';

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('model', InputArgument::REQUIRED, 'The name of model will be created.'),
            array('module', InputArgument::OPTIONAL, 'The name of module will be used.'),
        );
    }

    /**
     * @return mixed
     */
    protected function getTemplateContents()
    {
        $module = $this->laravel['modules']->findOrFail($this->getModuleName());
        $path   = __DIR__.'/stubs/repository.stub';

        return Stub::createFromPath($path, [
	            'NAME'              => $this->getRepositoryName(),
	            'NAMESPACE'         => $this->getClassNamespace($module),
	            'CLASS'             => $this->getClass(),
	            'MODEL'				=> $this->argument('model'),
	            'LOWER_NAME'        => $module->getLowerName(),
	            'ENTITY_NAMESPACE'	=> $this->laravel['modules']->config('paths.generator.model'),
	            'MODULE'            => $this->getModuleName(),
	            'STUDLY_NAME'       => $module->getStudlyName(),
	            'MODULE_NAMESPACE'  => $this->laravel['modules']->config('namespace'),
        ])->render();
    }

    public function getClass(){
    	
    	if(str_contains($class = parent::getClass(), $subfix = 'Repository')){
    		return $class;
    	}

    	return $class.$subfix;
    }

    /**
     * @return mixed
     */
    protected function getDestinationFilePath()
    {
        $path = $this->laravel['modules']->getModulePath($this->getModuleName());

        $seederPath = $this->laravel['modules']->config('paths.generator.repository');

        return $path.$seederPath.'/'.$this->getRepositoryName().'.php';
    }

    /**
     * @return mixed|string
     */
    private function getRepositoryName()
    {
        return Str::studly($this->argument('model').'Repository');
    }

    /**
     * Get default namespace.
     *
     * @return string
     */
    public function getDefaultNamespace()
    {
        return $this->laravel['modules']->config('paths.generator.repository');
    }
}
