<?php 
namespace FWTA\Core\Console\Commands;

use FWTA\Core\Console\Commands\Command;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

/**
 * Translate the the content of lang() function call.
 *
 * @author SoS
 */
class ThemeCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature =  'fwta:copy-theme 
                                {to? : Copy theme to}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy the default admin to application';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $destinationPath = $this->argument('to') ?: config('core.themes.to');
        $coreModule = $this->laravel["modules"]->findOrFail("core");
        $themeDestination = $coreModule->getPath()."/Resources/themes";

        $this->cleanUp($themeDestination); //Clean up useless files
        $this->files->copyDirectory($themeDestination, $destinationPath);

        $this->info("All themes is move to application ({$destinationPath})");

    }

    /**
     * Clean up the vendor and node_modules
     * 
     * @param  string $themeDestination
     * @return void
     */
    protected function cleanUp($themeDestination){

        foreach ($this->files->directories($themeDestination) as $theme) {
            $themeName = basename($theme) ;
            foreach ($this->files->directories($theme) as $dir) {
                
                if(Str::contains($dir, 'node_modules')){
                    $this->files->deleteDirectory($dir);
                    $this->info("Remove node_modules from {$themeName} theme.");
                }
                if(Str::contains($dir, 'assets') && 
                    $this->files->exists($vendor = $dir."/vendor")
                ){
                    $this->files->deleteDirectory($vendor);
                    $this->info("Remove vendor from {$themeName} theme.");
                }
            }
        }
    }
}
