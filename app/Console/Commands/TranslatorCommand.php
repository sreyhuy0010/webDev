<?php 
namespace FWTA\Core\Console\Commands;

use FWTA\Core\Console\Commands\Command;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

/**
 * Translate the the content of lang() function call.
 *
 * @author SoS
 */
class TranslatorCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature =  'fwta:translator 
                                {paths? : paths to scan}
                                {filename=locale.php : filename to write the translation}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect all lang(\'text\') to transform to array';
    protected $translation = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->generateTranslationFile($this->getTranslation());
    }

    /**
     * Get translation text.
     *
     * @return string
     */
    public function translation()
    {
        return $this->translation;
    }

    /**
     * Get all files with the given directory, recursively.
     *
     * @param $path directory
     *
     * @return array \Symfony\Component\Finder\SplFileInfo;
     */
    protected function getFiles($path)
    {
         $this->info($path);
        return iterator_to_array(
            \Symfony\Component\Finder\Finder::create()->files()
                ->notPath($path."/Console")
                ->notPath($path.'/resources/lang')
                ->notPath($path.'/config')
                ->notPath($path.'/vendor')
                ->notPath($path.'/storage')
                ->notPath($path.'/modules')
                ->notPath($path.'/Providers')
                ->notPath($path.'/node_modules')
                ->ignoreDotFiles(true)
                ->in($path)->name('*.php'), 
            false);
    }

    /**
     * Get function lang('foo').
     *
     * @param $content
     *
     * @return array
     */
    public function getFunctionLangContent($content)
    {
        $pattern = "|(lang).*?\(.*?[\'\\\"].+?[\'\\\"].*?\)|";

        preg_match_all($pattern, $content, $matches);

        return $matches[0];
    }

    /**
     * Get the text between single or double quotes.
     *
     * @param string $content
     *
     * @return string
     */
    public function getText($content)
    {
        $pattern_text = '#(["\'])((?:(?!\1)[^\\\\]|(?:\\\\)*\\[^\\\\])*)\1#';

        preg_match($pattern_text, $content, $texts);

        return count($texts) ? $texts[2] : '';
    }

    /**
     * Generate translation file.
     *
     * @param string $content
     * @param string $locale_path
     */
    protected function generateTranslationFile($content)
    {
        $filename = $this->argument('filename');
        $content = <<<EOF
<?php
    return [

$content
    ];
EOF;

        collect($this->config->get('core.locales'))->each(function($locale) use($content, $filename){
            \File::makeDirectory(base_path()."/resources/lang/{$locale}", 0775, true, true);
            $locale_path = base_path()."/resources/lang/{$locale}/{$filename}";
            $this->files->put($locale_path, $content);
            $content = `awk '!a[$0]++' $locale_path`; //remove duplication
            $this->files->put($locale_path, $content);
            $this->info('Translations are written to '.$locale_path);
        });
        
    }

    /**
     * Laravel application.
     *
     * @return \Illuminate\Foundation\Application
     */
    protected function app()
    {
        return $this->getLaravel();
    }

    /**
     * Get the tranlation content from given path.
     *
     * @param array|null $paths
     *
     * @return string
     */
    public function getTranslation(array $paths = null)
    {
        $paths = $paths ?: $this->paths();

        $template = "\t\t'%s' => '%s',".PHP_EOL;
        
        foreach ($paths as $path) {
            foreach ($this->getFiles($path) as $file) {
                $function_lang_matches = $this->getFunctionLangContent(
                    $this->files->get($file->getPathName())
                );

                foreach ($function_lang_matches as $content) {
                    $text = $this->getText($content);
                    if (!empty($text)) {
                        $this->translation .= sprintf($template, $text, $text);
                    }
                }
            }
        }

        return $this->translation;
    }

    /**
     * @param array $defined_paths
     *
     * @return array|mixed
     */
    public function paths(array $defined_paths = [])
    {
        if ($defined_paths) {
            return $defined_paths;
        }

        $modules = collect(app('modules')->all())->map(function($module){
            return $module->getPath();
        })->toArray();

        return array_merge(
            $this->config->get('view.paths'),
            [base_path()],
            $modules
        ) ?: [];
    }
}
