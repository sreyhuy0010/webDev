<?php

namespace FWTA\Core\Console\Commands\Laravel;

use Illuminate\Foundation\Console\ModelMakeCommand as BaseModelMakeCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class ModelMakeCommand extends BaseModelMakeCommand{
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/model.stub';
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if (parent::fire() !== false) {
            
            if ($this->option('repository')) {
                $rootNamespace = $this->laravel->getNamespace();
                $model = class_basename($this->argument('name'));
                $this->call('make:repository', ['name' => $rootNamespace."Repositories\\{$model}Repository", 'model' => $this->argument('name')]);
            }

            if ($this->option('migration')) {
                $table = Str::plural(Str::snake(class_basename($this->argument('name'))));

                $this->call('make:migration', ['name' => "create_{$table}_table", '--create' => $table]);
            }
        }
    }

    protected function getOptions()
    {
        return [
            ['migration', 'm', InputOption::VALUE_NONE, 'Create a new migration file for the model.'],
            ['repository', 'r', InputOption::VALUE_NONE, 'Create a new repository file for the model.'],
        ];
    }
}
