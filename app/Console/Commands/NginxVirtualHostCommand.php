<?php namespace FWTA\Core\Console\Commands;

use FWTA\Core\Console\Commands\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Process;

/**
 * Generate a vertual host for nginx
 * 
 * @author Sophy SEM <sophy.s@firstwomentechasia.com> 
 */
class NginxVirtualHostCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'fwta:vhost {server} {root?} {--phpversion=7} {--port=80} {--sslport=443}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate nginx virtual host';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
	    $this->process = new Process($this->buildCommand());
        $this->process->setTimeout(3600);
        $this->info($this->buildCommand());
        $this->process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                $this->error($buffer);
            } else {
                $this->info($buffer);
            }
        });
            
        if (!$this->process->isSuccessful()) {
            throw new \RuntimeException($this->process->getErrorOutput());
        }
    }

    protected function buildCommand(){

        $core_path = app('modules')->find('core')->getPath();
        $command   = $core_path ."/Console/bin/nginx.php".$this->option('phpversion').".sh";
        $port      = $this->option('port');
        $server    = $this->argument('server');
        $root      = $this->argument('root') ?: base_path('public');
        $sslport   = $this->option('sslport');

        return "sudo {$command} {$server} {$root} {$port} {$sslport}";
    }
}
