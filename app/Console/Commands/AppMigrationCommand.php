<?php 
namespace FWTA\Core\Console\Commands;

use FWTA\Core\Console\Commands\Command;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

/**
 * Translate the the content of lang() function call.
 *
 * @author SoS
 */
class AppMigrationCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'fwta:app-migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate the orders of module for application';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        collect(config('core.migration_orders'))->each(function($module){
            if($module == 'app') $this->call('migrate', ['--force' => true]);
            else $this->call('module:migrate', compact('module') + ['--force' => true]);
        });
    }
}
