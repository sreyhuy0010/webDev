<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'frontend.index', 'uses' => 'Frontend\HomeController@index']);
Route::get('expert/{slug}.html',[
	'as' 	=> 'frontend.expertises.detail',
	'uses' 	=> 'Frontend\ExpertiseController@detail'
]);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('post/{slug}.html',[
	'as'	=> 'frontend.post.detail',
	'uses'	=> 'Frontend\PostController@detail'
	]);

Route::get('team/{id}-{title}.html',[
	'as' 	=> 'frontend.team.detail',
	'uses' 	=> 'Frontend\TeamWorkController@detail'
]);

Route::group(['prefix' => 'subscriber', 'middleware' => 'web'], function(){
	Route::post('save', [
		'as' 	=> 'subscriber.save',
		'uses' 	=> 'Frontend\SubscriberController@store'
	]);
});

Route::group(['prefix' => 'p'], function(){

	Route::get('/', [
		'as' 	=> 'frontend.pages',
		'uses' 	=> 'Frontend\PageController@getAllPage'
	]);

	Route::get('{slug}.html', [
		'as' 	=> 'frontend.page.detail',
		'uses' 	=> 'Frontend\PageController@getPage'
	]);
});
Route::get('expertise.html',[
	'as' 	=> 'frontend.expertise',
	'uses' 	=> 'Frontend\ExpertiseController@index'
]);

Route::get('news-and-events.html',[
	'as' 	=> 'frontend.news-and-events',
	'uses' 	=> 'Frontend\PostController@index'
]);

Route::get('contact-us.html',[
	'as' 	=> 'frontend.contact-us',
	'uses' 	=> 'Frontend\ContactUsController@index'
]);

Route::post('sendmail', ['as' => 'post.send.mail', 'uses' => 'Frontend\MailController@send']);


Route::get('galleries.html',[
	'as' 	=> 'frontend.galleries',
	'uses' 	=> 'Frontend\GalleryController@index'
]);

